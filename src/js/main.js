$(document).ready(function(){
	initSlider();
});
// Функции
// Слайдер на главной
function initSlider(){
	$('#slider ul.slider-list').bxSlider({
		speed: 1000,
		moveSlides: 1,
		responsive: false,
		mode: 'fade',
		controls: false,
	});
}