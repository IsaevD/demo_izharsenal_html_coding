'use strict';
// Инициализация плагинов
var gulp = require('gulp'), // Сам Gulp JS
	sass = require('gulp-sass'), // Препроцессор Sass
	notify = require('gulp-notify'), // Нотификатор
	plumber = require('gulp-plumber'), // Формирует вывод об ошибке при этом работа Gulp не прерывается
	livereload = require('gulp-livereload'); // Плагин LiveReload для Gulp

// Задачи для sass
// Сборка и компрессия Sass
gulp.task('sass', function(){ // Таск "sass"
	gulp.src('src/css/*.scss') // Путь к файлу
		.pipe(plumber({ // Формирует вывод об ошибке
				errorHandler: notify.onError("<%= error.message %>")
		}))
		.pipe(sass({outputStyle: 'expanded'})) // Что именно делаем. В данном случае компилируем и подвергаем компрессии
		.pipe(gulp.dest('src/css')) // Куда складываем результат
		.pipe(notify("CSS изменен!")); // Выводим сообщение об успешном изменении
});

// Задачи для html
// Нотификация при изменении html
gulp.task('notify-html', function(){
	gulp.src('src/*.html')
		.pipe(notify("HTML изменен!"));
});

// Запуск слежки за изменениями в файлах
gulp.task('watch', function(){
	livereload.listen();
	gulp.watch('src/css/*.scss', ['sass']); // При изменении файлов .scss запускать задачу sass
	gulp.watch('src/css/*.css').on('change', livereload.changed); // При изменении в файлах .css запускать livereload
	gulp.watch('src/*.html', ['notify-html']).on('change', livereload.changed); // При изменении в файлах .html запускать задачу  notify-html и затем livereload
	gulp.watch('src/js/*.js').on('change', livereload.changed); // При изменении в файлах .js запускать livereload
});

// Запуск задач по умолчанию
gulp.task('default', ['sass', 'watch']);